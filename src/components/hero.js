import React from "react"
import PropTypes from "prop-types"
import {
  generateFakeId,
  generateFakeIdByAge,
} from "south-african-fake-id-generator"
import Button from "../components/button"
import headerImage from "../images/header.png"
import logo from "../images/xyz.png"

const ShowBox = ({ children }) => {
  return (
    <p
      style={{
        border: "1px solid #80808040",
        fontSize: "18px",
        width: "300px",
        fontWeight: "700",
        letterSpacing: "4px",
        color: "rgb(79 90 99)",
        background: "rgb(246 246 250)",
      }}
    >
      {" "}
      {children}
    </p>
  )
}
const Input = ({ value, ...rest }) => {
  return (
    <input
      style={{
        margin: "5px",
        padding: "15px",
        border: "2px solid #80808040",
        borderRadius: "5px",
        background: "#fff",
        fontSize: "18px",
        width: "300px",
        fontWeight: "700",
        letterSpacing: "4px",
        color: "#6c7984",
      }}
      value={value}
      {...rest}
    />
  )
}
class Header extends React.Component {
  constructor() {
    super()
    this.state = { fakeId: "", fakeAgeId: "" }
    this.setFakeId = this.setFakeId.bind(this)
    this.setFakeAgeId = this.setFakeAgeId.bind(this)
    this.setFakeAge = this.setFakeAge.bind(this)
  }

  setFakeId() {
    const fakeId = generateFakeId()
    this.setState({ fakeId })
  }
  setFakeAgeId() {
    if (this.state.fakeAge) {
      const fakeAgeId =
        this.state.fakeAge.length !== 0
          ? generateFakeIdByAge(this.state.fakeAge)
          : ""
      this.setState({ fakeAgeId })
    }
  }
  setFakeAge(e) {
    console.log("setting fake age:", e.target.value)
    this.setState({ fakeAge: e.target.value })
  }

  render() {
    return (
      <div
       className="main-container"
        style={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          padding: "0.5rem 1rem",
        }}
      >
        <div
          style={{
            backgroundImage: `url(${headerImage})`,
            position: "absolute",
            top: 0,
            zIndex: -5,
            height: "100vh",
            width: "100vw",
            opacity: 0.5,
          }}
        />
        <div class="logo-container">
          <a href="https://touch.net.za">
            <img src={logo} alt="Midas Touch Logo" />
          </a>
        </div>
        <div class="info-container">
          <div class="info">
            <h1 style={{ textAlign: "center", fontSize: "1.85rem" }}>
              {" "}
              DummySAID API 🇿🇦{" "}
            </h1>
            <p style={{ textAlign: "center", maxWidth: 440 }}>
              This DummySAID API is a REST API built with NodeJS & Express. The
              main purpose of this API is to validate and generate
              13 digit South African ID numbers. A while ago
               the Western Governement shared an article into the structure
              of a South African ID, this inspired us to play around with this information and create a tool
              that will help South African developers. There's a number of NPM odules that allow
              you to parse and validate a South African ID but none that allow
              you to generate a fake one. So we decided to create our own. This
              package can help developer's validate user data or even create
              dummy scenarios for testing purposes.
            </p>
          </div>
          <div className="cta-buttons">
            <div class="cta-button-container">
              <p>View REST API Docs here</p>
              <a href="https://dummysaid.touch.net.za"><Button>View Docs </Button></a>
            </div>
            <div class="cta-button-container">
              <p>Check out NPM Module Here</p>
              <a href="https://www.npmjs.com/package/south-african-fake-id-generator"><Button>Vist NPM</Button></a>
            </div>
            <div class="cta-button-container">
              <p>Contribute to Repo</p>
              <a href="https://gitlab.com/midas-touch/dummysaid-api"><Button>Gitlab Repo</Button></a>
            </div>
          </div>
        </div>
        <div className="demo">
          <div className="demo-item-container">
            <div className="title">
              <p>Generate fake Id</p>
            </div>
            <div className="body">
              <Button onClick={this.setFakeId}>Generate</Button>
            </div>
            <div className="footer">
              <ShowBox>{this.state.fakeId}</ShowBox>
            </div>
          </div>
          <div className="demo-item-container">
            <div className="title">
              <p>Generate fake Id using age</p>
            </div>
            <div className="body">
              <Input
                onChange={this.setFakeAge}
                value={this.state.fakeAge}
                placeholder="Enter age"
              />
              <Button onClick={this.setFakeAgeId}>Generate</Button>
            </div>
            <div className="footer">
              <ShowBox>{this.state.fakeAgeId}</ShowBox>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

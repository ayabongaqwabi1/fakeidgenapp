import React from "react"
import PropTypes from "prop-types"

import { COLORS } from "../styles/constants"

const Footer = ({ siteTitle }) => (
  <footer
    style={{
      padding: "1rem",
      backgroundColor: COLORS.lightGray,
    }}
  >
    <div
      style={{
        display: "grid",
        alignItems: "center",
        justifyContent: "space-between",
        padding: "1rem 2rem",
        fontSize: ".85rem",
      }}
    >
      <div style={{ color: COLORS.blue, fontWeight: 700 }}>
        <a
          style={{ textDecoration: "none", color:"#56a9c3" }}

          href="https://github.com/ayabongaqwabi/south-african-fake-id-generator"
        >
          View Github Repo
        </a>
      </div>
      <div style={{ color: COLORS.gray }}>
        © {new Date().getFullYear()}
        {` `}
        <a style={{ color: COLORS.gray, textDecoration: "none" }} href="https://www.linkedin.com/in/ayabongaqwabi/">Ayabonga Qwabi </a>| <a style={{ color: COLORS.gray, textDecoration: "none" }} href="https://touch.net.za">Midas Touch Technologies</a> | {siteTitle}
      </div>
    </div>
  </footer>
)

Footer.propTypes = {
  siteTitle: PropTypes.string,
}

Footer.defaultProps = {
  siteTitle: ``,
}

export default Footer

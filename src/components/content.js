import React from "react"

import feature from "../images/feature.png"
import SectionHeader from "./section-header"
import { COLORS } from "../styles/constants"

const Content = () => (
  <div style={{ padding: "4rem 1rem", textAlign: "center" }}>
    <SectionHeader
      title="Minimal Features"
      description="Don't spend time ripping out unneeded plugins and bloat."
    />
    <content
      style={{
        display: "grid",
        alignItems: "center",
        justifyContent: "center",
        gridTemplateColumns: "repeat(auto-fit, minmax(240px, 340px))",
      }}
    >
      <div>


        <h3>How to use this library?</h3>
        <p style={{ color: COLORS.gray }}>
          Download the library from NPM using the following command in a terminal:
          <code> npm install --save south-african-fake-id-generator</code>
        </p>
        <h3>Usage In NodeJS</h3>
        <code>
          var fakeSaIdGenerator = require('south-african-fake-id-generator');

          // Generate valid random id number
          var fakeId = fakeSaIdGenerator.generateFakeId();

          // Generate invalid random id number
          var fakeId = fakeSaIdGenerator.generateInvalidFakeId();

          // Generate valid random id number by age
          var fakeId = fakeSaIdGenerator.generateFakeIdByAge("23");

          // Generate invalid random id number by age
          var fakeId = fakeSaIdGenerator.generateInvalidFakeIdByAge("78");

          // Check if Id number is valid
          var fakeId = fakeSaIdGenerator.isValid("9701286633088");  // false
        </code>
      </div>
    </content>
  </div>
)

export default Content

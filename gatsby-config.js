module.exports = {
  siteMetadata: {
    title: `DummiySAID 🇿🇦 | Midas Touch Technologies `,
    description: `A playground to generate fake SA ID numbers by implementing the DummySAID API. Validate a South African Id number with us online. Build a fake  id number online. Support for RSA, South Africa, ZA Identity numbers. Get ID nmber based on Age or gender`,
    author: `@ayabongaqwabi`,
    siteUrl: `https://said.touch.net.za`,
    image: "https://saiddocs.touch.net.za/css/dummysaid.png"
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "YOUR_GOOGLE_ANALYTICS_TRACKING_ID",
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
  ],
}
